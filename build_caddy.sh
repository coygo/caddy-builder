#! /bin/sh

set -e

basedir=$(dirname "$(realpath $0)")

GOPATH="$basedir" go build -ldflags="-s -w" -tags="$plugins" -v caddy
