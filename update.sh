#! /bin/sh

set -e

basedir="$(dirname $(realpath $0))"

"$basedir/update_vendor.sh"
"$basedir/update_src.sh"
