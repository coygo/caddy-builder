#! /bin/sh

set -e

basedir="$(dirname $(realpath $0))"
srcdir="$(realpath $basedir/src/caddy)"

cd "$srcdir"
go clean -cache -testcache -modcache
rm go.mod go.sum
