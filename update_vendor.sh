#! /bin/sh

set -e

basedir="$(dirname $(realpath $0))"
srcdir="$(realpath $basedir/src/caddy)"

rm -f "$srcdir/go.mod" "$srcdir/go.sum"

cp "$basedir/go.mod.template" "$srcdir/go.mod"

cd "$srcdir"
go mod download
go mod vendor

ver="$(grep 'github.com/caddyserver/caddy' $srcdir/go.mod | sed 's/^\s*//;s/\s*$//' | awk '{ print $2 }')"
sum="$(grep 'github.com/caddyserver/caddy '$ver' ' $srcdir/go.sum | awk '{ print $3 }' )"
updated_at="$(date -u +%FT%TZ)"
printf "\
Info:
  Version: \"$ver\"
  Sum:     \"$sum\"
Updated At: $updated_at
" >"$basedir/info.yaml"
printf "{
  \"info\": {
    \"version\": \"$ver\",
    \"sum\": \"$sum\"
  },
  \"updated_at\": \"$updated_at\"
}
" >"$basedir/info.json"
