package main

import (
	"time"

	"github.com/caddyserver/caddy/caddy/caddymain"
	"github.com/caddyserver/caddy/telemetry"
)

func main() {
	// Disable telemetry
	caddymain.EnableTelemetry = false
	go func() {
		// It's wired that Caddy always StartEmitting since v1.0.0.
		// So we have to StopEmitting ugly like this ;)
		time.Sleep(1 * time.Second)
		telemetry.StopEmitting()
	}()

	// Caddy Go!
	caddymain.Run()
}
