module caddy

go 1.12

replace github.com/h2non/gock => gopkg.in/h2non/gock.v1 v1.0.14

require (
	github.com/BTBurke/caddy-jwt v3.7.1+incompatible
	github.com/SchumacherFM/mailout v1.3.0
	github.com/Xumeiquer/nobots v0.1.1
	github.com/abiosoft/caddy-git v0.0.0-20190703061829-f8cc2f20c9e7
	github.com/caddyserver/caddy v1.0.4
	github.com/caddyserver/dnsproviders v0.4.0
	github.com/caddyserver/forwardproxy v0.0.0-20191002195248-247c0bafaabd
	github.com/captncraig/caddy-realip v0.0.0-20190710144553-6df827e22ab8
	github.com/captncraig/cors v0.0.0-20190703115713-e80254a89df1
	github.com/casbin/caddy-authz v1.0.2
	github.com/casbin/casbin v1.9.1 // indirect
	github.com/coopernurse/caddy-awslambda v1.0.0
	github.com/echocat/caddy-filter v0.14.0
	github.com/emersion/caddy-wkd v0.0.0-20190709145130-bdb1e7dbb218
	github.com/emersion/go-openpgp-wkd v0.0.0-20191011220651-01af8781ec9b // indirect
	github.com/epicagency/caddy-expires v1.1.1
	github.com/freman/caddy-reauth v0.0.0-20191025011741-deaa60e56872
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/hacdias/caddy-minify v1.0.2
	github.com/hacdias/caddy-service v1.0.1
	github.com/hacdias/caddy-webdav v1.1.0
	github.com/improbable-eng/grpc-web v0.11.0 // indirect
	github.com/juju/ratelimit v1.0.1 // indirect
	github.com/jung-kurt/caddy-cgi v1.11.4
	github.com/jung-kurt/caddy-pubsub v0.5.6
	github.com/kodnaplakal/caddy-geoip v0.0.0-20190820062741-c06787a76821
	github.com/linkonoid/caddy-dyndns v0.0.0-20190718171622-2414d6236b0f
	github.com/mastercactapus/caddy-proxyprotocol v0.0.3
	github.com/miekg/caddy-prometheus v0.0.0-20190709133612-1fe4cb19becd
	github.com/mmcloughlin/geohash v0.9.0 // indirect
	github.com/nicolasazrak/caddy-cache v0.3.4
	github.com/payintech/caddy-datadog v0.0.0-20190812115610-44482c4c027a
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pieterlouw/caddy-grpc v0.1.0
	github.com/pieterlouw/caddy-net v0.1.5
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/pteich/caddy-tlsconsul v0.0.0-20190901182037-6acb4a557d88
	github.com/pyed/ipfilter v1.1.4
	github.com/quasoft/memstore v0.0.0-20191010062613-2bce066d2b0b // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/steambap/captcha v1.3.0 // indirect
	github.com/tarent/loginsrv v1.3.1
	github.com/techknowlogick/caddy-s3browser v0.0.0-20191115222008-ed8124a2104f
	github.com/tinylib/msgp v1.1.0 // indirect
	github.com/xuqingfeng/caddy-rate-limit v1.6.6
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.19.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
)
