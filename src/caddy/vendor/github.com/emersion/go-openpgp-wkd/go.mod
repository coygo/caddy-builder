module github.com/emersion/go-openpgp-wkd

go 1.13

require (
	github.com/tv42/zbase32 v0.0.0-20160707012821-501572607d02
	golang.org/x/crypto v0.0.0-20180910181607-0e37d006457b
)
