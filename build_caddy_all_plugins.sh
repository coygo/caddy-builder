#! /bin/sh

set -e

basedir=$(dirname "$(realpath $0)")
srcdir=$(realpath "$basedir"/src/caddy)
plugins=
for tag in $(grep '+build' "$srcdir"/plugin_*.go | sed 's/^.* +build //'); do
	plugins="$plugins $tag"
done

export plugins
"$basedir"/build_caddy.sh
