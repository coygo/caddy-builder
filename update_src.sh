#! /bin/sh

set -e

basedir="$(dirname $(realpath $0))"
srcdir="$(realpath $basedir/src/caddy)"
caddymaindir="$(realpath $srcdir/vendor/github.com/caddyserver/caddy/caddy/caddymain)"

patch "$caddymaindir/run.go" "$basedir/run.go.patch"

ver="$(jq -r .info.version $basedir/info.json)"
sum="$(jq -r .info.sum $basedir/info.json)"
sed "s|_VER_|$ver|;s|_SUM_|$sum|" "$basedir/version.go.template" >"$caddymaindir/version.go"
